### What is this repository for? ###

* Sample project for TakeAway, the app lists restaurants fetched from a json file.
 users can browse, search, filter and favourite restaurants from the list.

### How do I get set up? ###

* To run the app, download this repository and use AndroidStudio with an emulator / device
or install the .apk file located under build/outputs/apk
* Configuration - restaurants data file located in `assets` folder
* To run tests, right click the java folder under `src/test/java` and select `Run Tests in Java`

### Project info ###
* Architecture: MVVM
* Language: Java
* 3rd party libraries used
    - Gson: https://github.com/google/gson/blob/master/LICENSE
    - Material Components https://github.com/material-components/material-components-android/blob/master/LICENSE
    - Tests: Robolectric, room-testing

### Implementation notes ###
* Restaurant names are used as database keys (hashed) under the assumption the data is unique
* Sorting values contains 8 options - based on the assumption its fixed size in data
* For simplicity & readability, Room operations done on main thread

### Who do I talk to? ###

* tomgolanx@gmail.com