package com.tomgolan.restaurants.config;

import com.tomgolan.restaurants.model.SortType;

import java.util.ArrayList;

public class AppConfig {

    public static final String RESTAURANTS_FILE_NAME = "restaurants.json";

    // Available sort options
    private static ArrayList<SortType> mSortOptions = new ArrayList<SortType>() {
        {
            add(SortType.RATING_AVG);
            add(SortType.DELIVERY_COST);
            add(SortType.MIN_COST);
            add(SortType.DISTANCE);
            add(SortType.NEWEST);
            add(SortType.POPULARITY);
            add(SortType.BEST_MATCH);
            add(SortType.AVG_PRODUCT_PRICE);
        }
    };

    public static ArrayList<SortType> getSortTypes() {
        return mSortOptions;
    }
}
