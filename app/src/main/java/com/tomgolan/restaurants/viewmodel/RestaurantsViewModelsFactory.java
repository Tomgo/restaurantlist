package com.tomgolan.restaurants.viewmodel;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.lang.reflect.InvocationTargetException;

public class RestaurantsViewModelsFactory implements ViewModelProvider.Factory {

    private String mFileName;
    private Context mContext;

    public RestaurantsViewModelsFactory(String fileName, Context applicationContext) {
        this.mFileName = fileName;
        this.mContext = applicationContext;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        try {
            return modelClass.getConstructor(String.class, Context.class).newInstance(mFileName, mContext);
        } catch (IllegalAccessException | InstantiationException
                | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("Cannot create an instance of " + modelClass, e);
        }
    }
}
