package com.tomgolan.restaurants.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.tomgolan.restaurants.comparators.RestaurantsComapratorFactory;
import com.tomgolan.restaurants.database.Favourite;
import com.tomgolan.restaurants.database.FavouritesDatabase;
import com.tomgolan.restaurants.model.Restaurant;
import com.tomgolan.restaurants.model.Restaurants;
import com.tomgolan.restaurants.model.SortType;
import com.tomgolan.restaurants.util.AssetsUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RestaurantsViewModel extends ViewModel {

    private Context mContext;
    private MutableLiveData<List<Restaurant>> mRestaurantLiveData = new MutableLiveData<>();

    public RestaurantsViewModel(String fileName, Context context) {
        mContext = context;
        List<Restaurant> restaurants = loadRestaurantsFromJson(fileName, context);
        initRestaurants(restaurants);
    }

    private List<Restaurant> loadRestaurantsFromJson(String fileName, Context context) {
        List<Restaurant> restaurantList = new ArrayList<>();
        try {
            String json = AssetsUtil.getJsonFromAssets(context, fileName);
            Restaurants restaurants = new Gson().fromJson(json, Restaurants.class);
            if (restaurants.getRestaurants() != null) {
                restaurantList = restaurants.getRestaurants();
            }
        } catch (Exception e) {
            Log.w("RestaurantsViewModel", e.getMessage());
        }
        return restaurantList;
    }

    private void initRestaurants(List<Restaurant> restaurants) {
        // load favourites
        for (Favourite favourite : getFavoritesDatabase().favouriresDao().getAllFavourites()) {
            for (Restaurant restaurant : restaurants) {
                if (restaurant.getId() == favourite.id) {
                    restaurant.setFavourite(true);
                    break;
                }
            }
        }

        // Sort
        sortByDefaultTypesAndOrder(restaurants);

        // Update
        mRestaurantLiveData.postValue(restaurants);
    }

    public LiveData<List<Restaurant>> getRestaurantLiveData() {
        return mRestaurantLiveData;
    }

    public void onSortTypeSelected(String sortOption) {
        SortType type = null;
        try {
            type = Enum.valueOf(SortType.class, sortOption);
        } catch (IllegalArgumentException e) {
            Log.w("RestaurantsViewModel", "No enum value: " + sortOption, e);
        }

        if (type != null) {
            //Sort
            Collections.sort(mRestaurantLiveData.getValue(), RestaurantsComapratorFactory.getCompartor(type));
            sortByDefaultTypesAndOrder(mRestaurantLiveData.getValue());

            // Update
            mRestaurantLiveData.setValue(mRestaurantLiveData.getValue());
        }
    }

    private void sortByDefaultTypesAndOrder(List<Restaurant> restaurants) {
        Collections.sort(restaurants, RestaurantsComapratorFactory.getCompartor(SortType.STATUS));
        Collections.sort(restaurants, RestaurantsComapratorFactory.getCompartor(SortType.FAVOURITES));

        // Order and set new position
        int position = 0;
        for (Restaurant restaurant : restaurants) {
            restaurant.setPosition(position);
            position++;
        }
    }

    public List<Restaurant> filterRestaurants(String filterText) {
        List<Restaurant> result = new ArrayList<>();
        for (Restaurant restaurant : mRestaurantLiveData.getValue()) {
            if (restaurant.getName().toLowerCase().contains(filterText.toLowerCase())) {
                result.add(restaurant);
            }
        }
        return result;
    }

    public void onFavoriteClicked(Restaurant restaurant, boolean isFavorite) {
        Favourite favourite = new Favourite(restaurant.getId());
        restaurant.setFavourite(isFavorite);
        if (isFavorite) {
            getFavoritesDatabase().favouriresDao().insertFavourite(favourite);
        } else {
            getFavoritesDatabase().favouriresDao().deleteFavourite(favourite);
        }
    }

    private FavouritesDatabase getFavoritesDatabase() {
        return FavouritesDatabase.getDatabase(mContext);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        FavouritesDatabase.closeDatabase();
    }
}
