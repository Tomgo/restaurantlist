package com.tomgolan.restaurants.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Favourite {
    @NonNull
    @PrimaryKey
    public int id;

    public Favourite(@NonNull int id) {
        this.id = id;
    }
}