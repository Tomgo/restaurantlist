package com.tomgolan.restaurants.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = Favourite.class, version = 1, exportSchema = false)
public abstract class FavouritesDatabase extends RoomDatabase {

    private static String TABLE_NAME = "Favourite";
    //define static instance
    private static FavouritesDatabase mInstance;

    //method to get room database
    public static FavouritesDatabase getDatabase(Context context) {

        if (mInstance == null)
            mInstance = Room.databaseBuilder(context.getApplicationContext(),
                    FavouritesDatabase.class, TABLE_NAME)
                    .allowMainThreadQueries()
                    .build();

        return mInstance;
    }

    //method to remove instance
    public static void closeDatabase() {
        mInstance = null;
    }

    //define note dao ( data access object )
    public abstract FavouritesDao favouriresDao();
}
