package com.tomgolan.restaurants.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.tomgolan.restaurants.database.Favourite;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FavouritesDao {
    @Query("SELECT * FROM Favourite")
    List<Favourite> getAllFavourites();

    @Insert(onConflict = REPLACE)
    void insertFavourite(Favourite favourite);

    @Delete
    void deleteFavourite(Favourite favourite);
}
