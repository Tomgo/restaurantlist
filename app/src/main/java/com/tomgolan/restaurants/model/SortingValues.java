package com.tomgolan.restaurants.model;

public class SortingValues {
    private double bestMatch;
    private double newest;
    private double ratingAverage;
    private long distance;
    private double popularity;
    private int averageProductPrice;
    private int deliveryCosts;
    private int minCost;

    public double getBestMatch() {
        return bestMatch;
    }

    public double getNewest() {
        return newest;
    }

    public double getRatingAverage() {
        return ratingAverage;
    }

    public long getDistance() {
        return distance;
    }

    public double getPopularity() {
        return popularity;
    }

    public int getAverageProductPrice() {
        return averageProductPrice;
    }

    public int getDeliveryCosts() {
        return deliveryCosts;
    }

    public int getMinCost() {
        return minCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortingValues that = (SortingValues) o;
        return Double.compare(that.bestMatch, bestMatch) == 0 &&
                Double.compare(that.newest, newest) == 0 &&
                Double.compare(that.ratingAverage, ratingAverage) == 0 &&
                distance == that.distance &&
                Double.compare(that.popularity, popularity) == 0 &&
                averageProductPrice == that.averageProductPrice &&
                deliveryCosts == that.deliveryCosts &&
                minCost == that.minCost;
    }
}
