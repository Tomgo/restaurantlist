package com.tomgolan.restaurants.model;

import com.google.gson.annotations.SerializedName;

public enum Status {
    @SerializedName(value = "closed")
    CLOSED("Closed"),
    @SerializedName(value = "order ahead")
    ORDER_AHEAD("Order ahead"),
    @SerializedName(value = "open")
    OPEN("Open");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
