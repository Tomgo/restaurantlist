package com.tomgolan.restaurants.model;


public class Restaurant {
    // position in list
    private int position;
    private String name;
    private Status status;
    private boolean isFavourite;
    public SortingValues sortingValues;

    public Restaurant() { }

    public Restaurant(Restaurant restaurant) {
        this.name = restaurant.name;
        this.status = restaurant.status;
        this.isFavourite = restaurant.isFavourite;
        this.sortingValues = restaurant.sortingValues;
    }

    public int getId() {
        return name.hashCode();
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return isFavourite == that.isFavourite &&
                name.equals(that.name) &&
                status == that.status &&
                sortingValues.equals(that.sortingValues);
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
