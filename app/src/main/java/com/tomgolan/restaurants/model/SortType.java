package com.tomgolan.restaurants.model;

public enum SortType {
    FAVOURITES("Favourites"),
    STATUS("Status"),
    BEST_MATCH("Best Match"),
    NEWEST("Newest"),
    RATING_AVG("Rating"),
    DISTANCE("Distance"),
    POPULARITY("Popularity"),
    AVG_PRODUCT_PRICE("Avg. Price"),
    DELIVERY_COST("Delivery Cost"),
    MIN_COST("Min.");

    private String mDisplayName;

    SortType(String displayName) {
        mDisplayName = displayName;
    }

    public String getDisplayName() {
        return mDisplayName;
    }
}
