package com.tomgolan.restaurants.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.tomgolan.restaurants.R;
import com.tomgolan.restaurants.adapter.RestaurantsAdapter;
import com.tomgolan.restaurants.model.SortType;
import com.tomgolan.restaurants.viewmodel.RestaurantsViewModel;
import com.tomgolan.restaurants.config.AppConfig;
import com.tomgolan.restaurants.databinding.FragmentRestaurantsBinding;
import com.tomgolan.restaurants.model.Restaurant;
import com.tomgolan.restaurants.viewmodel.RestaurantsViewModelsFactory;

import java.util.ArrayList;
import java.util.List;

import static com.tomgolan.restaurants.config.AppConfig.RESTAURANTS_FILE_NAME;

public class RestaurantsFragment extends Fragment implements RestaurantsAdapter.IFavouriteListener {

    private FragmentRestaurantsBinding mBinding;
    private RestaurantsAdapter mRestaurantsAdapter;
    private RestaurantsViewModel mRestaurantsViewModel;
    private Observer<List<Restaurant>> mRestaurantsObserver;

    public static RestaurantsFragment newInstance() {
        return new RestaurantsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RestaurantsViewModelsFactory viewModelsFactory = new RestaurantsViewModelsFactory(RESTAURANTS_FILE_NAME, getActivity().getApplicationContext());
        mRestaurantsViewModel = new ViewModelProvider(getActivity(), viewModelsFactory).get(RestaurantsViewModel.class);
        mRestaurantsObserver = this::onRestaurantsChanged;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_restaurants, container, false);
        initRecyclerView();
        initSearchView();
        initChips();
        mRestaurantsViewModel.getRestaurantLiveData().observe(this, mRestaurantsObserver);
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRestaurantsViewModel.getRestaurantLiveData().removeObserver(mRestaurantsObserver);
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = mBinding.restaurantsRecycler;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);

        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRestaurantsAdapter = new RestaurantsAdapter(this);
        recyclerView.setAdapter(mRestaurantsAdapter);
    }

    private void initSearchView() {
        SearchView searchView = mBinding.searchView;
        searchView.setOnClickListener(v -> onSearchViewClicked());
        searchView.setOnSearchClickListener(v -> onSearchIconClicked());
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onQueryTextChanged(newText);
                return true;
            }
        });

        searchView.setOnCloseListener(() -> {
            onSearchViewClosed();
            return false;
        });
    }

    private void onSearchViewClosed() {
        toggleChips(true);
    }

    private void onSearchIconClicked() {
        toggleChips(false);
    }

    private void onSearchViewClicked() {
        mBinding.searchView.setIconified(false);
        toggleChips(false);
    }

    private void toggleChips(boolean show) {
        mBinding.chipsLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void onQueryTextChanged(String newText) {
        mRestaurantsAdapter.onRestaurantsFiltered(mRestaurantsViewModel.filterRestaurants(newText));
        scrollRecylcerToTop();
    }

    private void initChips() {
        ChipGroup chipGroup = mBinding.chipGroup;

        // init text and tag
        ArrayList<SortType> sortTypes = AppConfig.getSortTypes();
        for (int i = 0; i < mBinding.chipGroup.getChildCount(); i++) {
            Chip chip = (Chip) mBinding.chipGroup.getChildAt(i);
            chip.setTag(sortTypes.get(i).toString());
            chip.setText(sortTypes.get(i).getDisplayName());
        }

        chipGroup.setOnCheckedChangeListener((group, i) -> {
            Chip chip = group.findViewById(i);
            if (chip != null) {
                onSortTypeSelected(chip.getTag().toString());
            }
        });
    }

    private void onSortTypeSelected(String sortType) {
        mRestaurantsViewModel.onSortTypeSelected(sortType);
    }

    private void onRestaurantsChanged(List<Restaurant> restaurants) {
        mBinding.setHasData(restaurants.size() > 0);
        mRestaurantsAdapter.updateRestaurants(restaurants);
        scrollRecylcerToTop();
    }

    private void scrollRecylcerToTop() {
        mBinding.restaurantsRecycler.scrollToPosition(0);
    }

    @Override
    public void onFavouriteClicked(Restaurant restaurant, boolean add) {
        mRestaurantsViewModel.onFavoriteClicked(restaurant, add);
    }
}
