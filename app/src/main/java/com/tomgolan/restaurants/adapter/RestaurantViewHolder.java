package com.tomgolan.restaurants.adapter;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tomgolan.restaurants.databinding.RestaurantItemBinding;
import com.tomgolan.restaurants.model.Restaurant;
import com.tomgolan.restaurants.model.Status;

import java.util.Locale;

public class RestaurantViewHolder extends RecyclerView.ViewHolder {

    private RestaurantItemBinding mRestaurantItemBinding;

    public RestaurantViewHolder(RestaurantItemBinding restaurantItemBinding) {
        super(restaurantItemBinding.getRoot());
        mRestaurantItemBinding = restaurantItemBinding;
    }

    public void bindData(final Restaurant restaurant, RestaurantsAdapter.IFavouriteListener favouriteListener) {
        mRestaurantItemBinding.name.setText(restaurant.getName());

        mRestaurantItemBinding.status.setText(restaurant.getStatus().getDisplayName());
        mRestaurantItemBinding.status.setTextColor(getStatusColor(restaurant.getStatus()));

        mRestaurantItemBinding.deliveryCost.setText(format("%s €",
                String.valueOf(restaurant.sortingValues.getDeliveryCosts())));

        mRestaurantItemBinding.distance.setText(getDistanceFormat(restaurant.sortingValues.getDistance()));

        mRestaurantItemBinding.minOrder.setText(format("min. %s €",
                String.valueOf(restaurant.sortingValues.getMinCost())));

        mRestaurantItemBinding.ratingBar.setRating((float) restaurant.sortingValues.getRatingAverage());

        mRestaurantItemBinding.favorite.setChecked(restaurant.isFavourite());
        mRestaurantItemBinding.favorite.setOnClickListener(v -> {
            boolean isChecked = mRestaurantItemBinding.favorite.isChecked();
            favouriteListener.onFavouriteClicked(restaurant, isChecked);
        });
    }

    private int getStatusColor(Status status) {
        switch (status) {
            case CLOSED:
                return getColor(android.R.color.holo_red_light);
            case ORDER_AHEAD:
                return getColor(android.R.color.holo_blue_light);
            case OPEN:
            default:
                return getColor(android.R.color.holo_green_light);
        }
    }

    private int getColor(int color) {
        return ContextCompat.getColor(mRestaurantItemBinding.getRoot().getContext(), color);
    }

    private String getDistanceFormat(long distance) {
        if (distance > 1000) {
            float km = (float) distance / 1000;
            String kmString = String.format(Locale.getDefault(), "%.1f", km);
            return format("%s km", kmString);
        } else {
            return format("%s m", String.valueOf(distance));
        }
    }

    private String format(String format, String value) {
        return String.format(Locale.getDefault(), format, value);
    }
}
