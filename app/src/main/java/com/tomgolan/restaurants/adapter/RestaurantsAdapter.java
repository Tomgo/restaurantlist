package com.tomgolan.restaurants.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tomgolan.restaurants.databinding.RestaurantItemBinding;
import com.tomgolan.restaurants.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

    private List<Restaurant> mRestaurants;
    private IFavouriteListener mFavoriteListener;

    public interface IFavouriteListener {
        void onFavouriteClicked(Restaurant restaurant, boolean add);
    }

    public RestaurantsAdapter(IFavouriteListener favoriteListener) {
        mRestaurants = new ArrayList<>();
        mFavoriteListener = favoriteListener;
    }

    public void onRestaurantsFiltered(List<Restaurant> restaurants) {
        mRestaurants.clear();
        mRestaurants.addAll(restaurants);
        notifyDataSetChanged();
    }

    public void updateRestaurants(List<Restaurant> restaurants) {
        final RestaurantsDiffCallback diffCallback = new RestaurantsDiffCallback(mRestaurants, restaurants);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        mRestaurants.clear();
        mRestaurants.addAll(restaurants);
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RestaurantItemBinding restaurantItemBinding =
                RestaurantItemBinding.inflate(layoutInflater, parent, false);
        return new RestaurantViewHolder(restaurantItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        holder.bindData(mRestaurants.get(position), mFavoriteListener);
    }

    @Override
    public int getItemCount() {
        return mRestaurants.size();
    }
}
