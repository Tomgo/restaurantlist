package com.tomgolan.restaurants.adapter;

import androidx.recyclerview.widget.DiffUtil;

import com.tomgolan.restaurants.model.Restaurant;

import java.util.List;

public class RestaurantsDiffCallback extends DiffUtil.Callback {

    private List<Restaurant> mNewRestaurants;
    private List<Restaurant> mOldRestaurants;

    public RestaurantsDiffCallback(List<Restaurant> newRestaurants, List<Restaurant> oldRestaurants) {
        mNewRestaurants = newRestaurants;
        mOldRestaurants = oldRestaurants;
    }

    @Override
    public int getOldListSize() {
        return mOldRestaurants.size();
    }

    @Override
    public int getNewListSize() {
        return mNewRestaurants.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mNewRestaurants.get(newItemPosition).getId() == mOldRestaurants.get(oldItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mNewRestaurants.get(oldItemPosition).getPosition() == mNewRestaurants.get(newItemPosition).getPosition()
                && mNewRestaurants.get(newItemPosition).equals(mOldRestaurants.get(oldItemPosition));
    }
}
