package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

import java.util.Comparator;

public abstract class RestaurantComparator implements Comparator<Restaurant> {

    @Override
    public int compare(Restaurant o1, Restaurant o2) {
        int result = compareValues(o1, o2);

        // Reverse order
        switch (result) {
            case 1:
                return -1;
            case -1:
                return 1;
            default:
                return 0;
        }
    }

    protected abstract int compareValues(Restaurant o1, Restaurant o2);
}
