package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class AvgPriceComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        return Integer.compare(o1.sortingValues.getAverageProductPrice(), o2.sortingValues.getAverageProductPrice());
    }
}
