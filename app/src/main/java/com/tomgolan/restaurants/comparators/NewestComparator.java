package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class NewestComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        return Double.compare(o1.sortingValues.getNewest(), o2.sortingValues.getNewest());
    }
}
