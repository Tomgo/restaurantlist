package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class RatingComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        return Double.compare(o1.sortingValues.getRatingAverage(), o2.sortingValues.getRatingAverage());
    }
}
