package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.SortType;

public class RestaurantsComapratorFactory {

    public static RestaurantComparator getCompartor(SortType type) {
        switch (type) {
            case BEST_MATCH:
                return new BestMatchComparator();
            case POPULARITY:
                return new PopularityComparator();
            case NEWEST:
                return new NewestComparator();
            case AVG_PRODUCT_PRICE:
                return new AvgPriceComparator();
            case RATING_AVG:
                return new RatingComparator();
            case DISTANCE:
                return new DistanceComparator();
            case DELIVERY_COST:
                return new DeliveryCostComparator();
            case MIN_COST:
                return new MinCostComparator();
            case STATUS:
                return new StatusComparator();
            case FAVOURITES:
            default:
                return new FavouritesComparator();
        }
    }
}
