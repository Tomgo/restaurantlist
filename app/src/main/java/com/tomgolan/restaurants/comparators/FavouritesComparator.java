package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class FavouritesComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        return Boolean.compare(o1.isFavourite(), o2.isFavourite());
    }
}
