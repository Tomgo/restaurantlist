package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class MinCostComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        // lower is better
        return Integer.compare(o2.sortingValues.getMinCost(), o1.sortingValues.getMinCost());
    }
}
