package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class StatusComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        int status1 = o1.getStatus().ordinal();
        int status2 = o2.getStatus().ordinal();
        return Integer.compare(status1, status2);
    }
}
