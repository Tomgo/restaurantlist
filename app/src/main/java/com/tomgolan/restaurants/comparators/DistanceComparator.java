package com.tomgolan.restaurants.comparators;

import com.tomgolan.restaurants.model.Restaurant;

public class DistanceComparator extends RestaurantComparator {

    @Override
    public int compareValues(Restaurant o1, Restaurant o2) {
        // Lower (closer) is better
        return Long.compare(o2.sortingValues.getDistance(), o1.sortingValues.getDistance());
    }
}
