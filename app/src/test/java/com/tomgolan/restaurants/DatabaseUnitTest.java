package com.tomgolan.restaurants;

import android.content.Context;
import android.os.Build;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import com.tomgolan.restaurants.comparators.StatusComparator;
import com.tomgolan.restaurants.config.AppConfig;
import com.tomgolan.restaurants.database.Favourite;
import com.tomgolan.restaurants.database.FavouritesDao;
import com.tomgolan.restaurants.database.FavouritesDatabase;
import com.tomgolan.restaurants.model.Restaurant;
import com.tomgolan.restaurants.model.Status;
import com.tomgolan.restaurants.viewmodel.RestaurantsViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.P)
public class DatabaseUnitTest {
    private Context context = ApplicationProvider.getApplicationContext();
    private FavouritesDao favouritesDao;
    private FavouritesDatabase db;

    @Before
    public void setUp() throws Exception {
        db = Room.inMemoryDatabaseBuilder(context, FavouritesDatabase.class)
                .allowMainThreadQueries()
                .build();
        favouritesDao = db.favouriresDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void insertFavouriteAndReadFromList() {
        Favourite favourite = new Favourite(1);
        favouritesDao.insertFavourite(favourite);
        List<Favourite> favourites = favouritesDao.getAllFavourites();
        Favourite savedFavourite = null;
        for (Favourite fav : favourites) {
            if (fav.id == favourite.id) {
                savedFavourite = fav;
                break;
            }
        }
        assertEquals(savedFavourite.id, favourite.id);
    }

    @Test
    public void insertAndRemoveFavoriteFromList() {
        Favourite favourite = new Favourite(1);
        favouritesDao.insertFavourite(favourite);
        assertEquals(favouritesDao.getAllFavourites().size(), 1);
        favouritesDao.deleteFavourite(favourite);
        assertEquals(favouritesDao.getAllFavourites().size(), 0);
    }
}