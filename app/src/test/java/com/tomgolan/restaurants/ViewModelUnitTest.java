package com.tomgolan.restaurants;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.tomgolan.restaurants.comparators.StatusComparator;
import com.tomgolan.restaurants.config.AppConfig;
import com.tomgolan.restaurants.database.FavouritesDatabase;
import com.tomgolan.restaurants.model.Restaurant;
import com.tomgolan.restaurants.model.SortType;
import com.tomgolan.restaurants.model.Status;
import com.tomgolan.restaurants.viewmodel.RestaurantsViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.P)
public class ViewModelUnitTest {
    private static final String APP_NAME = "Restaurants";

    private Context context = ApplicationProvider.getApplicationContext();
    private RestaurantsViewModel mRestaurantsViewModel;

    @Before
    public void setUp() throws Exception {
        //MockitoAnnotations.initMocks(this);
        mRestaurantsViewModel = new RestaurantsViewModel(AppConfig.RESTAURANTS_FILE_NAME, context);
    }

    @Test
    public void valid_context() {
        System.out.println(context.getString(R.string.app_name));
        assertEquals(context.getString(R.string.app_name), APP_NAME);
    }

    @Test
    public void validate_json_parsing() {
        mRestaurantsViewModel.getRestaurantLiveData().observeForever(restaurants ->
                assertEquals(restaurants.size(), 19));
    }

    @After
    public void clean() throws Exception {
        //mRestaurantsViewModel.closeDatabase();
        FavouritesDatabase.closeDatabase();
    }

    @Test
    public void status_comparator() {
        StatusComparator statusComparator = new StatusComparator();
        Restaurant restaurant = new Restaurant();
        restaurant.setStatus(Status.ORDER_AHEAD);

        Restaurant restaurant1 = new Restaurant();
        restaurant1.setStatus(Status.CLOSED);
        int result = statusComparator.compareValues(restaurant, restaurant1);
        System.out.println("status_comparator: " + result);
        assertEquals(result, 1);
    }

    @Test
    public void testStatusAndFavouriteSorting() {
        mRestaurantsViewModel.getRestaurantLiveData().observeForever(restaurants ->
                assertEquals(restaurants.get(18).getName(), "Zenzai Sushi"));
    }

    @Test
    public void testDistanceSorting() {
        mRestaurantsViewModel.getRestaurantLiveData().observeForever(restaurants ->
                assertEquals(restaurants.get(0).getName(), "Tanoshii Sushi"));
        mRestaurantsViewModel.onSortTypeSelected(SortType.DISTANCE.toString());
    }

    @Test
    public void testFavouriteClick() {
        Restaurant restaurant = mRestaurantsViewModel.getRestaurantLiveData().getValue().get(0);
        mRestaurantsViewModel.onFavoriteClicked(restaurant, true);
        mRestaurantsViewModel.getRestaurantLiveData().observeForever(restaurants ->
                assertEquals(restaurants.get(0).isFavourite(), true));
    }
}